const amqp = require('amqplib');
class Amqp {
  constructor(queue) {
    this.queue = queue;
    this.url = 'amqp://localhost';
  }

  open() {
    amqp.connect(this.url)
      .then(conn => conn.createChannel())
      .then((ch) => {
        this.channel = ch;
        return ch.assertQueue(this.queue);
      });
  }

  publish(msg) {
    const messageString = JSON.stringify(msg);
    return this.channel.sendToQueue(this.queue, new Buffer(messageString)); // eslint-disable-line no-buffer-constructor
  }

  consume(promise) {
    this.channel.consume(this.queue, (msg) => {
      promise(msg)
      .then(() => {
        this.channel.ack(msg);
      })
      .catch(() => {
        this.sendNack(msg);
      });
    });
  }

  sendNack(msg) {
    this.channel.nack(msg);
  }
    // function (msg) {
    //   if (msg !== null) {
    //     console.log(msg.content.toString());
    //     ch.ack(msg);
    //   }
    // });
  }
}
