const express = require('express');
// const APIError = require('../lib/apierror');

const router = express.Router();
const packagesCtrl = require('../controllers/packages.controller');
// const libCache = require('../lib/cache');

// function getCachePackage(req, res, next) {
//   libCache.getCache(req.params.name)
//   .then((pkg) => {
//     console.log('entra get cache');
//     if (pkg) {
//       console.log('cache');
//       return res.json(pkg);
//     }
//     return next();
//   });
// }

// function setPackage(req, res, next) {
//   libCache.setPackage().then();
// }

router.route('/')
  .get((req, res, next) => {
    return packagesCtrl.list(req.query)
      .then(res.json.bind(res))
      .catch(next);
  })
  .post((req, res, next) => {
    const response = req.body;
    return packagesCtrl.create(response)
      .then(res.json.bind(res))
      .catch(next);
  });

router.route('/:name')
  .get((req, res, next) => {
    const name = req.params.name; // eslint-disable-line prefer-destructuring
    return packagesCtrl.get(name)
      .then(res.json.bind(res))
      .catch(next);
  });

// function resovePackageAndNext(req, res, next) {
//   packagesCtrl.get(req.params.name)
//     .then((data) => {
//       req.bbcache = data;
//       next();
//     })
//     .catch(error => next(error));
// }

// function cachedPackageAndResponse(req, res, next) {
//   libCache.saveCache(req.bbcache)
//     .then(() => res.json(req.bbcache))
//     .catch(error => next(error));
// }

module.exports = router;
