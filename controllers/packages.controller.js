const packageModel = require('../models/packages.model');
const APIError = require('../lib/apierror');

function list(query) {
  return packageModel.list(query);
}

function get(name) {
  return new Promise((resolve, reject) => {
    if (!name) {
      return reject(new APIError('no params name'));
    }
    return packageModel.get(name).then(resolve);
  });
}

function create(pkg) {
  return packageModel.create(pkg);
}

module.exports = {
  list,
  get,
  create,
};
