const _ = require('lodash');
const fs = require('fs');
const util = require('util');

const fsReadfile = util.promisify(fs.readFile);
// getPackageJson('lodash').then(console.log);

function convertStringToJson(data) {
  return JSON.parse(data);
}

function parserData(data) {
  return {
    name: _.get(data, 'name'),
    description: data.description,
    version: data.version,
    license: data.license,
  };
}

function getPackageJson(nameFolder) {
  const file = `./node_modules/${nameFolder}/package.json`;
  return fsReadfile(file, 'utf8')
    .then(convertStringToJson)
    .then(parserData)
    .catch(() => null);
}

module.exports = getPackageJson;
