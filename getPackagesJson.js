const fs = require('fs');
const util = require('util');

const fsReadFolder = util.promisify(fs.readdir);
const FOLDER = './node_modules';
const getPackageJson = require('./readFile');
const _ = require('lodash');

function fillTerPackagesNull(array) {
  return _.filter(array, element => element !== null);
}


function resolveVersion(folder) {
  return getPackageJson(folder);
}

function getPackagesFromList(list) {
  const promises = [];
  list.forEach((element) => {
    promises.push(resolveVersion(element));
  });
  return Promise.all(promises);
}

function getListFolders() {
  return fsReadFolder(FOLDER);
}

function getPackagesJson() {
  return getListFolders()
    .then(getPackagesFromList)
    .then(fillTerPackagesNull);
}
getPackagesJson().then(console.log);
module.exports = getPackagesJson;
