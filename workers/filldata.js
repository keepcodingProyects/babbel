const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/test');
const db = require('../db');
const P = require('bluebird');

const Package = mongoose.model('Package', {
  name: String,
  description: String,
  version: String,
  license: String,
});

P.each(db, element => new Package(element).save())
.then(console.log);
