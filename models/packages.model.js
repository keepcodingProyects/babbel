const mongoose = require('mongoose');
const APIError = require('../lib/apierror');

const Package = mongoose.model('Package', {
    name: String,
    description: String,
    version: String,
    license: String,
});

function list({ license, version }) {
  const query = {};
  if (license) {
    query.license = license;
  }

  if (version) {
    query.version = version;
  }
  return Package.find(query);
}

function get(name) {
  return Package.findOne({ name });
}

function create(pkg) {
  if (!pkg.name || !pkg.version) {
    return new Promise((resolve, reject) => { // eslint-disable-line arrow-body-style
      return reject(new APIError('No name or version'));
    });
  }
  return Package.findOne({ name: pkg.name })
    .then((existsPkg) => {
      if (existsPkg) {
       throw new APIError('Same name');
      }
      return new Package(pkg).save();
    });
}
module.exports = {
  list,
  get,
  create,
};
