const express = require('express');
const APIError = require('./lib/apierror');

const app = express();
const bodyParser = require('body-parser');
const packagesRouter = require('./routes/packgages.route');
const mongoose = require('mongoose');
const util = require('util');

mongoose.connect('mongodb://localhost/test');

if (process.env.MONGO_DEBUG) {
  mongoose.set('debug', (collectionName, method, query, doc) => {
    const msg = `${collectionName}.${method}`;
    console.log(msg, util.inspect(query, false, 20), doc);
  });
}

app.use(bodyParser.json({ limit: '50mb' }));

app.use('/packages', packagesRouter);

app.use((req, res, next) => {
  console.log('entra');
  return next();
});

app.use((error, req, res, next) => {
  console.log('erorr');
  if (error instanceof APIError) {
    return res.status(error.status).json({ message: error.message });
  }
  return next(error);
});

app.use((error, req, res) => {
  console.error(error);
  return res.status(500).json({ message: 'uknown' });
});


app.listen(3000, () => console.log('Escuchando en el puerto 3000'));
