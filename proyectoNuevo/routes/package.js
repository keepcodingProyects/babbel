const express = require('express');
const router = express.Router();
const cache = require('../cache');

const packageCtrl = require('../controllers/package.controller');

function getGlobal(route, url) {
  if (url === '/') {
    return route;
  }
  return `${route}${url}`;
}

router.get('/', (req, res, next) => {
  packageCtrl.list()
  .then((result) => {
    const url = getGlobal('/packages', req.url);
    // console.log('cache route', cache);
    cache[url] = result;
    res.json(result);
  })
  .catch(next);
});

router.post('/', (req, res, next) => {
  packageCtrl.create(req.body)
    .then((response) => {
      res.json(response);
    })
    .catch((err) => {
      next(err);
    });
});

module.exports = router;
