const mongoose = require('mongoose');

const Package = mongoose.model('Package', {
  name: String,
  description: String,
  version: String,
  license: String,
});

function list() {
  return Package.find({});
}

function create(pkg) {
  const packageModel = new Package(pkg);
  return packageModel.save()
    .catch((error) => {
      if (error.code === 11000) {
        const err = new Error('Duplicate');
        err.status = 411;
        throw err;
      }
      throw error;
    });
}
module.exports = {
  list,
  create,
};
